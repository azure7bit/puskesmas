<?php
 ini_set('display_errors', 'On');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }

  include('header.php');

?>


<div id="page">
  <div id="content">
    <div class="box">
      <?php if ($_SESSION['level'] != 'dokter') { ?>
      <?php } //endif ?>
      <div class="box" id="content-box1">
        <form name="absen_form" id="absen_form" method="post" enctype="multipart/form-data" data-remote="true">
          <h4>Statistik berdasarkan
            <select name="mydropdown" class="styled" id="mydropdown">
              <option value="satu" selected="">Jumlah Pasien Periksa</option>
              <option value="dua" >HotList Penyakit </option>
              <option value="tiga" >Rentang Umur Pasien</option>
              <option value="empat" >Perbandingan per Poliklinik</option>
            </select>
          </h4>

          <div class="box" id="content-box1">
            <form name="absen_form" id="absen_form" method="post" enctype="multipart/form-data" data-remote="true">
              Tahun
              <select name="tahun" class="styled" id="tahun">
                <?php for ($i = 2013; $i <= 2015; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>

              Bulan
              <select name="bulan_awal" class="styled" id="bulan_awal">
                <option value=1>01</option>
                <option value=2>02</option>
                <option value=3>03</option>
                <option value=4>04</option>
                <option value=5>05</option>
                <option value=6>06</option>
                <option value=7>07</option>
                <option value=8>08</option>
                <option value=9>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
              -
              <select name="bulan_akhir" class="styled" id="bulan_akhir">
                <option value=1>01</option>
                <option value=2>02</option>
                <option value=3>03</option>
                <option value=4>04</option>
                <option value=5>05</option>
                <option value=6>06</option>
                <option value=7>07</option>
                <option value=8>08</option>
                <option value=9>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
            <td><input type="submit" name ="submit" value="Submit" class="btn btn-success"></td>
          </form>

          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>

        <script type="text/javascript" src="http://localhost/puskesmas/js/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function() {
            $("#absen_form").submit(function(e){
              var opt = $('#mydropdown').val();
              var postData = $(this).serializeArray();
              var awal = parseInt($('#bulan_awal').val());
              var akhir = parseInt($('#bulan_akhir').val());
              if(awal >= akhir){
                 alert('Bulan awal tidak boleh lebih besar daripada bulan akhir');
                 e.preventDefault();
                 e.stopPropagation();
                 return false;
              }else{
                if(opt=="satu"){
                  var options = {
                    chart: {
                      renderTo: 'container',
                      type: 'column'
                    },
                    title: {
                      text: "Jumlah Pasien Poliklinik Bulan "+ $('#bulan_awal').val() + " - " + $('#bulan_akhir').val()+", Tahun "+$('#tahun').val()
                    },
                    xAxis: {
                      categories: []
                    },
                    yAxis: {
                      min: 0,
                      title: {
                        text: 'Jumlah Pasien'
                      }
                    },
                    tooltip: {
                      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} Pasien </b></td></tr>',
                      footerFormat: '</table>',
                      shared: true,
                      useHTML: true
                    },
                    plotOptions: {
                      column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                      }
                    },
                    series: [{}]
                  };
                  $.ajax({
                    url : 'get_statistik1.php',
                    type: "POST",
                    data : postData,
                    success:function(obj, textStatus, jqXHR){
                      if($.isEmptyObject(obj[0])==false){
                        options.categories = [];
                        for (var i = 0; i < obj.length; i++){
                          for(var x = 0; x < obj[0].category.length; x++){
                            options.xAxis.categories.push(obj[0].category[x]);
                          }
                        }
                        options.series = obj;
                        var chart = new Highcharts.Chart(options);
                      }else{
                        var chart = new Highcharts.Chart(options);
                        for (var i = 0; i < chart.series.length; i++){
                          chart.series[i].setData([]);
                        }
                      }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                      var chart = new Highcharts.Chart(options);
                      for (var i = 0; i < chart.series.length; i++){
                        chart.series[i].setData([]);
                      }
                    }
                  });
                  e.preventDefault();
                  e.stopPropagation();
                }
                else if(opt=="dua"){
                  var options = {
                    chart: {
                      renderTo: 'container',
                      type: 'bar'
                    },
                    title: {
                      text: 'Data Penyakit terbanyak'
                    },
                    xAxis: {
                      categories: [],
                      title: {
                        text: null
                      }
                    },
                    yAxis: {
                      min: 0,
                      title: {
                        text: 'Jumlah Penderita',
                        align: 'high'
                      },
                      labels: {
                        overflow: 'justify'
                      }
                    },
                    tooltip: {
                      valueSuffix: ' penderita'
                    },
                    plotOptions: {
                      bar: {
                        dataLabels: {
                          enabled: true
                        }
                      }
                    },
                    legend: {
                      layout: 'vertical',
                      align: 'right',
                      verticalAlign: 'top',
                      x: -40,
                      y: 100,
                      floating: true,
                      borderWidth: 1,
                      backgroundColor: '#FFFFFF',
                      shadow: true
                    },
                    credits: {
                      enabled: false
                    },
                    series: [{}]
                  };
                  $.ajax({
                    url : 'get_statistik2.php',
                    type: "POST",
                    data : postData,
                    success:function(obj, textStatus, jqXHR){
                      if($.isEmptyObject(obj[0])==false){
                        options.categories = [];
                        for (var i = 0; i < obj.length; i++){
                          for(var x = 0; x < obj[0].category.length; x++){
                            options.xAxis.categories.push(obj[0].category[x]);
                          }
                        }
                        options.series = obj;
                        var chart = new Highcharts.Chart(options);
                      }else{
                        var chart = new Highcharts.Chart(options);
                        for (var i = 0; i < chart.series.length; i++){
                          chart.series[i].setData([]);
                        }
                      }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                      var chart = new Highcharts.Chart(options);
                      for (var i = 0; i < chart.series.length; i++){
                        chart.series[i].setData([]);
                      }
                    }
                  });
                  e.preventDefault();
                  e.stopPropagation();
                }
                else if(opt=="tiga"){
                  var options = {
                    chart: {
                      renderTo: 'container',
                      plotBackgroundColor: null,
                      plotBorderWidth: null,
                      plotShadow: false,
                      type: 'pie',
                    },
                    title: {
                      text: 'Grafik Rentang Umur Pasien'
                    },
                    tooltip: {
                      pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                      pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                          enabled: true,
                          color: '#000000',
                          connectorColor: '#000000',
                          format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                        }
                      }
                    },
                    series: [{}]
                  };
                  $.ajax({
                    url : 'get_statistik.php',
                    type: "POST",
                    data : postData,
                    success:function(obj, textStatus, jqXHR){
                      if(obj!=null){
                        options.series = [];
                        for (var i = 0; i < obj.length; i++){
                          options.series.push({
                            data: obj
                          })
                        }
                        var chart = new Highcharts.Chart(options);
                      }else{
                        var chart = new Highcharts.Chart(options);
                        for (var i = 0; i < chart.series.length; i++){
                          chart.series[i].setData([]);
                        }
                      }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                      var chart = new Highcharts.Chart(options);
                      for (var i = 0; i < chart.series.length; i++){
                        chart.series[i].setData([]);
                      }
                    }
                  });
                  e.preventDefault();
                  e.stopPropagation();
                }
                else if(opt=="empat"){
                  var options = {
                    chart: {
                      renderTo: 'container',
                      type: 'column'
                    },
                    title: {
                      text: 'Jumlah Pasien tiap Poliklinik per Bulan'
                    },
                    xAxis: {
                      categories: []
                    },
                    yAxis: {
                      min: 0,
                      title: {
                        text: 'Jumlah Pasien'
                      }
                    },
                    tooltip: {
                      headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                          '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                      footerFormat: '</table>',
                      shared: true,
                      useHTML: true
                    },
                    plotOptions: {
                      column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                      }
                    },
                    series: [{}]
                  };
                  $.ajax({
                    url : 'get_statistik4.php',
                    type: "POST",
                    data : postData,
                    success:function(obj, textStatus, jqXHR){
                      if(obj!=null){
                        options.series = [];
                        for (var i = 0; i < obj.length; i++){
                          options.series.push({
                            name: obj[i].name,
                            data: [obj[i].data]
                          })
                        }
                        var chart = new Highcharts.Chart(options);
                      }else{
                        var chart = new Highcharts.Chart(options);
                        for (var i = 0; i < chart.series.length; i++){
                          chart.series[i].setData([]);
                        }
                      }
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                      var chart = new Highcharts.Chart(options);
                      for (var i = 0; i < chart.series.length; i++){
                        chart.series[i].setData([]);
                      }
                    }
                  });
                  e.preventDefault();
                  e.stopPropagation();
                }
              }
            })
          });
        </script>
      </head>
      <body>
        <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
        <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      </body>
    </div>
      <a href="_statistik.php" style="float:right;margin-right: 15px;"><input type="button" value="<<kembali" class="btn btn-success"></a>
    </div>
  </div>
  <div class="box" id="content-box2"></div><br class="clearfix" />
  </div>
  <div id="sidebar">
  <br class="clearfix" />
</div>
<?php include('footer.php');?>