<?php
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }

  include('header.php');

  $arr_nama_nama_bulan = "";
  foreach($arr_total_pas as $nama => $val){
    $arr_nama_nama_bulan[] = $nama;
  }

?>

<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
    window.open(url);
    }
  }

  window.onload = function () {
    (function () {
      $("#absen_form").submit();
    }());
  };
</script>

<div id="page">
  <div id="content">
    <div class="box">
      <?php if ($_SESSION['level'] != 'dokter') { ?>
      <?php } //endif ?>
      <div class="box" id="content-box1">
        <form name="absen_form" id="absen_form" method="post" enctype="multipart/form-data" action="_statistik_load.php">
          <h4>Statistik berdasarkan
            <select name="mydropdown" class="styled" id="mydropdown">
              <option value="satu">Jumlah Pasien Periksa</option>
              <option value="dua" >HotList Penyakit </option>
              <option value="tiga" >Rentang Umur Pasien</option>
              <option value="empat" selected="">Perbandingan per Poliklinik</option>
            </select>
          </h4>

          <div class="box" id="content-box1">
            <form name="absen_form" method="post" enctype="multipart/form-data" action="_statistik.php">
              Tahun
              <select name="tahun" class="styled">
                <?php for ($i = 2013; $i <= 2015; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>

              Bulan
              <select name="bulan_awal" class="styled">
                <option value=1>01</option>
                <option value=2>02</option>
                <option value=3>03</option>
                <option value=4>04</option>
                <option value=5>05</option>
                <option value=6>06</option>
                <option value=7>07</option>
                <option value=8>08</option>
                <option value=9>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
              -
              <select name="bulan_akhir" class="styled">
                <option value=1>01</option>
                <option value=2>02</option>
                <option value=3>03</option>
                <option value=4>04</option>
                <option value=5>05</option>
                <option value=6>06</option>
                <option value=7>07</option>
                <option value=8>08</option>
                <option value=9>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
            <td><input type="submit" name ="submit" value="Submit" class="btn btn-success"></td>
          </form>

          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>

        <script type="text/javascript" src="http://localhost/puskesmas/js/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function() {
            var options = {
              chart: {
                renderTo: 'container',
                type: 'column'
              },
              title: {
                text: 'Jumlah Pasien tiap Poliklinik per Bulan'
              },
              xAxis: {
                categories: ["<?php echo join('","', $arr_nama_nama_bulan); ?>"]
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Jumlah Pasien'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{}]
            };
            $("#absen_form").submit(function(e){
              var opt = $('#mydropdown').val();
              var postData = $(this).serializeArray();
              if(opt=="empat"){
                $.ajax({
                  url : 'get_statistik4.php',
                  type: "POST",
                  data : postData,
                  success:function(obj, textStatus, jqXHR){
                    if(obj!=null){
                      options.series = [];
                      for (var i = 0; i < obj.length; i++){
                        options.series.push({
                          name: obj[i].name,
                          data: [obj[i].data]
                        })
                      }
                      var chart = new Highcharts.Chart(options);
                    }else{
                      var chart = new Highcharts.Chart(options);
                      for (var i = 0; i < chart.series.length; i++){
                        chart.series[i].setData([]);
                      }
                    }
                  },
                  error: function(jqXHR, textStatus, errorThrown){
                    var chart = new Highcharts.Chart(options);
                    for (var i = 0; i < chart.series.length; i++){
                      chart.series[i].setData([]);
                    }
                  }
                });
                e.preventDefault();
                e.stopPropagation();
              }
            })
          });
        </script>
      </head>
      <body>
        <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
        <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
      </body>
    </div>
      <a href="_statistik.php" style="float:right;margin-right: 15px;"><input type="button" value="<<kembali" class="btn btn-success"></a>
    </div>
  </div>
  <div class="box" id="content-box2"></div><br class="clearfix" />
  </div>
  <div id="sidebar">
  <br class="clearfix" />
</div>
<?php include('footer.php');?>