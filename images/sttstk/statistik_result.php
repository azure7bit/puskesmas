<?php
  @session_start();

  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }

  include('../../header.php');

  $arr_nama_nama_bulan = "";
  foreach($arr_total_pas as $nama => $val){
    $arr_nama_nama_bulan[] = $nama;
  }

  $list = join(",",$arr_total_pas);
?>


<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }
</script>

<div id="page">
  <div id="content">
    <div class="box">
    <?php if ($_SESSION['level'] != 'dokter') { ?>
    <?php } //endif ?>
      <div class="box" id="content-box1">
        <form name="absen_form" method="post" enctype="multipart/form-data" action="_statistik_load.php">
          <h4>Statistik berdasarkan
            <select name="mydropdown" class="styled" onChange="document.location = this.value" value="GO">
              <option value="http://localhost/puskesmas/statistik.php" selected ="">Jumlah Pasien</option>
              <option value="http://localhost/puskesmas/statistik2.php" >Perbandingan per Poliklinik </option>
              <option value="http://localhost/puskesmas/statistik3.php" >Rentang Umur Pasien</option>
              <option value="http://localhost/puskesmas/statistik4.php" >HotList Penyakit  </option>
            </select>
          </h4>
          <div class="box" id="content-box1">
          <form name="absen_form" method="post" enctype="multipart/form-data" action="statistik5.php">
            Tahun <select name="tahun" class="styled">
              <?php for ($i = 2013; $i <= 2014; $i++) {
                echo "<option value=$i>$i</option>";
              }?>
            </select>

            Bulan <select name="bulan_awal" class="styled">
              <option value=01>01</option>
              <option value=02>02</option>
              <option value=03>03</option>
              <option value=04>04</option>
              <option value=05>05</option>
              <option value=06>06</option>
              <option value=07>07</option>
              <option value=08>08</option>
              <option value=09>09</option>
              <?php for ($i = 10; $i <= 12; $i++) {
                echo "<option value=$i>$i</option>";
              }?>
            </select>
            - <select name="bulan_akhir" class="styled">
                <option value=01>01</option>
                <option value=02>02</option>
                <option value=03>03</option>
                <option value=04>04</option>
                <option value=05>05</option>
                <option value=06>06</option>
                <option value=07>07</option>
                <option value=08>08</option>
                <option value=09>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
            <td><input type="submit" name ="submit" value="Submit" class="btn btn-success"></td>
          </form>

          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
          $(function () {
            $('#container').highcharts({
              chart: {
                type: 'column'
              },
              title: {
                text: '<? echo "Jumlah Pasien Poliklinik Bulan $bulan - $bulan_2 Tahun $tahun"?>'
              },
              xAxis: {
                categories: ["<?php echo join('","', $arr_nama_nama_bulan); ?>"]
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Jumlah Pasien'
                }
              },
              tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} Pasien </b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
              },
              plotOptions: {
                column: {
                  pointPadding: 0.2,
                  borderWidth: 0
                }
              },
              series: [{
                name: 'Total',
                data: [<?php echo $list?>]
              }]
            });
          });
        </script>

        </head>
        <body>
          <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
          <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </body>
      </div>
      <a href="_statistik.php" style="float:right;margin-right: 15px;"><input type="button" value="<<kembali" class="btn btn-success"></a>
    </div>
  </div>
  <div class="box" id="content-box2"></div>
  <br class="clearfix" />
</div>
<div id="sidebar"><br class="clearfix" /></div>
<?php include('footer.php');?>