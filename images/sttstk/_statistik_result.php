<?php
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
  $cnt = $_SESSION['cnt'];
  echo $cnt;
?>

<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }
</script>

<div id="page">
  <div id="content">
    <div class="box">
    <?php if ($_SESSION['level'] != 'dokter') { ?>
    <?php } //endif ?>
      <div class="box" id="content-box1">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript"src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
          $(function () {
            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: '<? echo "Jumlah Pasien Poliklinik Bulan $bulan Tahun $tahun"?>'
                },
                xAxis: {
                    categories: [
                        '<?php echo "$bulan"?>'
                    ]
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Jumlah Pasien'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Total',
                    data: [<?php echo $cnt?>]
                }]
            });
          });
        </script>
        </head>
        <body>
          <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
          <scriptsrc="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </body>
      </div>
    </div>
  </div>
  <div class="box" id="content-box2"></div>
  <br class="clearfix" />
</div>
<div id="sidebar"><br class="clearfix" /></div>
<?php include('footer.php');?>