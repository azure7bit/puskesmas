<?php
  include ('koneksi.php');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<body>
  <div id="page">
    <div id="content">
      <div class="box">
      <?php if ($_SESSION['level'] != 'dokter') { ?>
      <?php } //endif ?>
        <div class="box" id="content-box1">
          <form name="absen_form" method="post" enctype="multipart/form-data"
            action="_statistik5.php">
            <h4>
              Bulan <select name="bulan" class="styled"
                >
                <option value=01>01</option>
                <option value=02>02</option>
                <option value=03>03</option>
                <option value=04>04</option>
                <option value=05>05</option>
                <option value=06>06</option>
                <option value=07>07</option>
                <option value=08>08</option>
                <option value=09>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
            </h4>
            <h4>
              Tahun <select name="tahun" class="styled"
                >
                <?php for ($i = 2013; $i <= 2014; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
            </h4>
            <input type="submit" name="submit" value=" Submit " />
          </form>
          <div id="container"style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
      </div>
    </div>
    <div class="box" id="content-box2"></div>
    <br class="clearfix" />
  </div>
  <div id="sidebar"><br class="clearfix" /></div>
  <?php include('footer.php');?>
</body>