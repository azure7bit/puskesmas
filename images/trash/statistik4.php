<?php
  include ('koneksi.php');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }
</script>

<div id="page">
  <div id="content">
    <div class="box">
      <?php if ($_SESSION['level'] != 'dokter') { ?>
      <?php } //endif ?>
      <div class="box" id="content-box1">
        <h4>Statistik berdasarkan
          <select name="mydropdown" class="styled" onChange="document.location = this.value" value="GO">
            <option value="http://mamaseyul.byethost24.com/statistik.php"  ="">Jumlah Pasien</option>
            <option value="http://mamaseyul.byethost24.com/statistik2.php" >Perbandingan per Poliklinik </option>
            <option value="http://mamaseyul.byethost24.com/statistik3.php" >Rentang Umur Pasien</option>
            <option value="http://mamaseyul.byethost24.com/statistik4.php" selected>HotList Penyakit  </option>
          </select>
        </h4>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
          $(function () {
            $('#container').highcharts({
              chart: {
                type: 'bar'
              },
              title: {
                text: 'Data Penyakit terbanyak'
              },
              xAxis: {
                categories: ['Penyakit1', 'Penyakit2', 'Penyakit3', 'Penyakit4', 'Penyakit5'],
                title: {
                  text: null
                }
              },
              yAxis: {
                min: 0,
                title: {
                  text: 'Jumlah Penderita',
                  align: 'high'
                },
                labels: {
                  overflow: 'justify'
                }
              },
              tooltip: {
                valueSuffix: ' penderita'
              },
              plotOptions: {
                bar: {
                  dataLabels: {
                    enabled: true
                  }
                }
              },
              legend: {
                layout: 'vertical',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true
              },
              credits: {
                enabled: false
              },
              series: [{
                name: 'Jumlah',
                data: [107, 31, 635, 203, 2]
              }, {
                name: 'Laki-laki',
                data: [133, 156, 947, 408, 6]
              }, {
                name: 'Perempuan',
                data: [973, 914, 4054, 732, 34]
              }]
            });
          });
        </script>
      <body>
        <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
        <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      </body>
    </div>
  </div>
</div>
<div class="box" id="content-box2"></div>
  <br class="clearfix" />
</div>
<div id="sidebar"><br class="clearfix" /></div>
<?php include('footer.php');?>