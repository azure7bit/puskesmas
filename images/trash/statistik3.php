<?php
  include ('koneksi.php');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }
</script>

<div id="page">
  <div id="content">
    <div class="box">
      <?php if ($_SESSION['level'] != 'dokter') { ?>
      <?php } //endif ?>
      <div class="box" id="content-box1">
        <h4>Statistik berdasarkan
          <select name="mydropdown" class="styled" onChange="document.location = this.value" value="GO">
            <option value="http://mamaseyul.byethost24.com/statistik.php"  >Jumlah Pasien </option>
            <option value="http://mamaseyul.byethost24.com/statistik2.php" ="">Perbandingan per Poliklinik</option>
            <option value="http://mamaseyul.byethost24.com/statistik3.php" selected>Rentang Umur Pasien</option>
            <option value="http://mamaseyul.byethost24.com/statistik4.php" >HotList Penyakit</option>
          </select>
        </h4>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
          $(function () {
            $('#container').highcharts({
              chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
              },
              title: {
                text: 'Grafik Rentang Umur Pasien'
              },
              tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
              },
              plotOptions: {
                pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                  }
                }
              },
              series: [{
                type: 'pie',
                name: 'Rentang Umur Pasien',
                data: [
                  ['Balita (0-5)',   15.0],
                  ['Anak-anak (5-10)',    26.8],
                  {
                    name: 'Remaja (10-20)',
                    y: 12.8,
                    sliced: true,
                    selected: true
                  },
                  ['Dewasa1 (20-30)',    8.5],
                  ['Dewasa2 (30-40)',    17.5],
                  ['Dewasa3 (40-50)',    20.5],
                  ['Lansia1 (50-55)',    25.5],
                  ['Lansia2 (55-...)',   30.5],
                ]
              }]
            });
          });
        </script>
      </head>
    <body>
      <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
      <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
      <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </body>

  </div>
</div>
</div>
<div class="box" id="content-box2"></div>
  <br class="clearfix" />
</div>
<div id="sidebar"><br class="clearfix" /></div>
<?php include('footer.php');?>