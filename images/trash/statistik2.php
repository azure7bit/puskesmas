<?php
  include ('koneksi.php');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }
</script>

<div id="page">
  <div id="content">
    <div class="box">
      <?php if ($_SESSION['level'] != 'dokter') { ?>
      <?php } //endif ?>
      <div class="box" id="content-box1">
        <h4>Statistik berdasarkan
          <select name="mydropdown" class="styled" onChange="document.location = this.value" value="GO">
            <option value="http://mamaseyul.byethost24.com/statistik.php"  >Jumlah Pasien </option>
            <option value="http://mamaseyul.byethost24.com/statistik2.php" selected="">Perbandingan per Poliklinik</option>
            <option value="http://mamaseyul.byethost24.com/statistik3.php" >Rentang Umur Pasien</option>
            <option value="http://mamaseyul.byethost24.com/statistik4.php" >HotList Penyakit</option>
          </select>
        </h4>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
        $(function () {
          $('#container').highcharts({
            chart: {
              plotBackgroundColor: null,
              plotBorderWidth: 0,
              plotShadow: false
            },
            title: {
              text: 'Poliklinik',
              align: 'center',
              verticalAlign: 'middle',
              y: 50
            },
            tooltip: {
              pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
              pie: {
                dataLabels: {
                  enabled: true,
                  distance: -50,
                  style: {
                    fontWeight: 'bold',
                    color: 'white',
                    textShadow: '0px 1px 2px black'
                  }
                },
                startAngle: -90,
                endAngle: 90,
                center: ['50%', '75%']
              }
            },
            series: [{
              type: 'pie',
              name: 'Poliklinik',
              innerSize: '50%',
              data: [
                ['Poliklinik Umum',   45.0],
                ['Poliklinik Lansia',       26.8],
                ['Poliklinik Gigi', 12.8],
                ['Poliklinik KIA/KB',    80.5],
              ]
            }]
          });
        });
      </script>
    </head>
    <body>
      <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
      <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
      <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </body>
  </div>
</div>
</div>
<div class="box" id="content-box2"></div><br class="clearfix" /></div>
<div id="sidebar"><br class="clearfix" /></div>
<?php include('footer.php');?>