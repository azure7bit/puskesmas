<?php
  include ('koneksi.php');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }
</script>

  <div id="page">
    <div id="content">
      <div class="box">
        <?php if ($_SESSION['level'] != 'dokter') { ?>
        <?php } //endif ?>
        <div class="box" id="content-box1">
          <h4>Statistik berdasarkan
            <select name="mydropdown" class="styled" onChange="document.location = this.value" value="GO">
              <option value="http://mamaseyul.byethost24.com/statistik.php"  selected="">Jumlah Pasien </option>
              <option value="http://mamaseyul.byethost24.com/statistik2.php" >Perbandingan per Poliklinik </option>
              <option value="http://mamaseyul.byethost24.com/statistik3.php" >Rentang Umur Pasien</option>
              <option value="http://mamaseyul.byethost24.com/statistik4.php" >HotList Penyakit</option>
            </select>
          </h4>

          <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
          <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
          <script type="text/javascript">
            $(function () {
              $('#container').highcharts({
                chart: {
                  type: 'column'
                },
                title: {
                  text: 'Jumlah Pasien tiap Poliklinik per Bulan'
                },
                xAxis: {
                  categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                },
                yAxis: {
                  min: 0,
                  title: {
                    text: 'Jumlah Pasien'
                  }
                },
                tooltip: {
                  headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                  pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                      '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                  footerFormat: '</table>',
                  shared: true,
                  useHTML: true
                },
                plotOptions: {
                  column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                  }
                },
                series: [{
                    name: 'Umum',
                    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
                  }, {
                    name: 'Lansia',
                    data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]
                  }, {
                    name: 'Gigi',
                  data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]
                  }, {
                    name: 'KIA/KB',
                    data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]
                }]
              });
            });
          </script>
        </head>
        <body>
          <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
          <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </body>
      </div>
    </div>
  </div>
  <div class="box" id="content-box2"></div>
    <br class="clearfix" />
  </div>
  <div id="sidebar">
    <br class="clearfix" />
  </div>
  <?php include('footer.php');?>