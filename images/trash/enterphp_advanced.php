<?php
  include ('koneksi.php');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  if (isset($_GET['poliklinik'])){
    $poliklinik=$_GET['poliklinik'];
  }else{
    if (($_SESSION['adminname'])=='Poliklinik Umum'){
      $poliklinik="Umum";
    } elseif (($_SESSION['adminname'])=='Poliklinik Lansia'){
      $poliklinik="Lansia";
    } elseif (($_SESSION['adminname'])=='Poliklinik Gigi'){
      $poliklinik="Gigi";
    } elseif (($_SESSION['adminname'])=='Poliklinik KIA/KB'){
      $poliklinik="KIA/KB";
    } else{
      $poliklinik="semua";
    }
  }
  include('header.php');

  if(isset($_GET['delperiksa'])){
    $query = mysql_query("DELETE FROM periksa WHERE noRegistrasi = '{$_GET['delperiksa']}'")or die('Error : ' . mysql_error());
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
  }
?>

<script language="JavaScript" type="text/javascript">
  function delperiksa(a){
    if (confirm("pasien batal periksa?")){
      window.location.href = 'enter.php?delperiksa=' + a;
    }
  }
</script>

<div id="page">
  <div id="content">
    <div class="box">
      <?php
        if ($_SESSION['level'] != 'dinas') {
        echo $_SESSION['adminname'];
      ?>
      <div class="box" id="content-box1">
        <div><h4>LIST MENUNGGU : </h4></div>
          <table class="table table-striped">
            <thead>
              <tr>
                <th width="5%" style="font-size: 13px;text-align:center">No</th>
                <th width="10%" style="font-size: 13px;text-align:center">No. Registrasi</th>
                <th width="20%" style="font-size: 13px;text-align:center">Nama Pasien</th>
                <th width="30%" style="font-size: 13px;text-align:center">Alamat</th>
                <th width="10%" style="font-size: 13px;text-align:center">Tanggal Lahir</th>
                <th width="5%" style="font-size: 13px;text-align:center">Poliklinik</th>
                <th width="10%" style="font-size: 13px;text-align:center">Batal?</th>
              </tr>
            </thead>
            <?php
              @$counter=$start;
              $result = mysql_query("select * from pasien pa, periksa pe WHERE pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");

              if ($poliklinik=='Umum'){
                $result = mysql_query("select * from pasien pa, periksa pe where pe.poliklinik='Umum' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
              } elseif ($poliklinik=='Lansia'){
                $result = mysql_query("select * from pasien pa, periksa pe where pe.poliklinik='Lansia' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
              } elseif ($poliklinik=='Gigi'){
                $result = mysql_query("select * from pasien pa, periksa pe where pe.poliklinik='Gigi' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
              } elseif ($poliklinik=='KIA/KB'){
                $result = mysql_query("select * from pasien pa, periksa pe where pe.poliklinik='KIA/KB' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
              } else{
                $result = mysql_query("select * from pasien pa, periksa pe where pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
              }

              while(@$row=mysql_fetch_array($result)){
                if ($_SESSION['level'] == 'dokter'){
                  $a = "<a href=rekam_medis.php?id=".$row['noRegistrasi'].">".$row['noRegistrasi']."</a>";
                  $b = "<a href=rekam_medis.php?id=".$row['noRegistrasi'].">".$row['namaPasien']."</a>";
                  $c = $row['Alamat'];
                  $f = $row['tglLahir'];
                  $g = $row['poliklinik'];
                  $counter++;
                  echo "<tr><td>$counter</td><td>$a</td><td>$b</td><td>$c</td><td style='text-align:center'>$f </td><td style='text-align:center'>$g</td></tr>";
                }
                elseif ($_SESSION['level'] == 'admin'){
                  $a = "<a href=edit_periksa.php?id=".$row['noRegistrasi'].">".$row['noRegistrasi']."</a>";
                  $b = "<a href=edit_periksa.php?id=".$row['noRegistrasi'].">".$row['namaPasien']."</a>";
                  $c = $row['Alamat'];
                  $f = $row['tglLahir'];
                  $g = $row['poliklinik'];
                  $counter++;
                  echo "<tr><td>$counter</td><td>$a</td><td>$b</td><td>$c</td><td style='text-align:center'>$f </td><td style='text-align:center'>$g</td>
                  <td style=color:#0F8C8C;>|<a href=\"javascript:delperiksa('$a')\"> batal</a> |</td></tr>";
                }
              }
            ?>
          <tbody align="" role="alert" aria-live="polite" aria-relevant="all">    </tbody>
        </table>
      </div>

      <?php
        }
        else {
          echo "Maaf Anda tidak bisa akses menu Pasien Masuk";
        }
      ?>
    </div>
  </div>
  <div class="box" id="content-box2"></div>
  <br class="clearfix" />
</div>

<div id="sidebar"><br class="clearfix" /></div>
<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }
</script>
<?php include('footer.php');?>