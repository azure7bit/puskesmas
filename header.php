<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <title>Aplikasi Sistem Informasi Puskesmas Cicalengka</title>
  <link rel="stylesheet" type="text/css" href="style.css" />
  <link rel="stylesheet" type="text/css" href="bootstrap.css" />
  <script type="text/javascript" src="js/calendar.js"></script>
</head>
<body>
  <div id="wrapper">
    <div id="header">
      <div id="logo">
        <h1><a href="#">Sistem Informasi Puskesmas Cicalengka Bandung</a></h1>
      </div>
      <div id="slogan"></div>
    </div>
    <div id="menu">
      <?php
        if(!isset($_SESSION['adminsession'])){
    			echo '<nav><ul></ul></nav>';
    		} else {
    	?>
      <nav>
    		<ul class="navpanel">
    			<li class="first current_page_item">
            <li><a href="registrasi.php">Registrasi Pasien </a></li>
    			  <a href="enter.php">Pasien Masuk</a>
          </li>
          <?php
    				if($_SESSION['level'] == 'dokter') {
    			?>
          <?php } ?>
          <li><a href="index.php">Profile (<?php echo $_SESSION['adminname']?>)</a></li>
          <li><a href="_statistik.php">Statistik</a></li>
          <li><a href="logout.php">Logout </a></li>
    		</ul>
      </nav>
      <?php } ?>
    	<br class="clearfix" />
    </div>
    <!-- javascript at the bottom for fast page loading -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
    <script type="text/javascript" src="js/jquery.sooperfish.js"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        $('ul.sf-menu').sooperfish();
      });
    </script>