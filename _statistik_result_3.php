<?php
  include('koneksi.php');

  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }

  include('header.php');

?>

<script type="text/javascript">
  function goToNewPage(dropdownlist){
    var url = dropdownlist.options(dropdownlist.selectedIndex).value;
    if (url != ""){
      window.open(url);
    }
  }

  window.onload = function () {
    (function () {
      $("#absen_form").submit();
    }());
  };
</script>

<div id="page">
  <div id="content">
    <div class="box">
      <?php if ($_SESSION['level'] != 'dokter') { ?>
      <?php } //endif ?>
      <div class="box" id="content-box1">
        <form name="absen_form" id="absen_form" method="post" enctype="multipart/form-data" action="_statistik_load.php">
          <h4>Statistik berdasarkan
            <select name="mydropdown" class="styled" id="mydropdown">
              <option value="satu">Jumlah Pasien Periksa</option>
              <option value="dua" >HotList Penyakit </option>
              <option value="tiga" selected="">Rentang Umur Pasien</option>
              <option value="empat">Perbandingan per Poliklinik</option>
            </select>
          </h4>

          <div class="box" id="content-box1">
            <form name="absen_form" method="post" enctype="multipart/form-data" action="_statistik.php">
              Tahun
              <select name="tahun" class="styled">
                <?php for ($i = 2013; $i <= 2015; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>

              Bulan
              <select name="bulan_awal" class="styled">
                <option value=1>01</option>
                <option value=2>02</option>
                <option value=3>03</option>
                <option value=4>04</option>
                <option value=5>05</option>
                <option value=6>06</option>
                <option value=7>07</option>
                <option value=8>08</option>
                <option value=9>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
              -
              <select name="bulan_akhir" class="styled">
                <option value=1>01</option>
                <option value=2>02</option>
                <option value=3>03</option>
                <option value=4>04</option>
                <option value=5>05</option>
                <option value=6>06</option>
                <option value=7>07</option>
                <option value=8>08</option>
                <option value=9>09</option>
                <?php for ($i = 10; $i <= 12; $i++) {
                  echo "<option value=$i>$i</option>";
                }?>
              </select>
            <td><input type="submit" name ="submit" value="Submit" class="btn btn-success"></td>
          </form>

          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>

        <script type="text/javascript" src="http://localhost/puskesmas/js/jquery.min.js"></script>
        <script type="text/javascript" src="http://localhost/puskesmas/statistik/jquery.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function() {
            var options = {
              chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
              },
              title: {
                text: 'Grafik Rentang Umur Pasien'
              },
              tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
              },
              plotOptions: {
                pie: {
                  allowPointSelect: true,
                  cursor: 'pointer',
                  dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                  }
                }
              },
              series: [{}]
            };
            $("#absen_form").submit(function(e){
              var opt = $('#mydropdown').val();
              var postData = $(this).serializeArray();
              if(opt=="tiga"){
                $.ajax({
                  url : 'get_statistik.php',
                  type: "POST",
                  data : postData,
                  success:function(obj, textStatus, jqXHR){
                    if(obj!=null){
                      options.series = [];
                      for (var i = 0; i < obj.length; i++){
                        options.series.push({
                          data: obj
                        })
                      }
                      var chart = new Highcharts.Chart(options);
                    }else{
                      var chart = new Highcharts.Chart(options);
                      for (var i = 0; i < chart.series.length; i++){
                        chart.series[i].setData([]);
                      }
                    }
                  },
                  error: function(jqXHR, textStatus, errorThrown){
                    var chart = new Highcharts.Chart(options);
                    for (var i = 0; i < chart.series.length; i++){
                      chart.series[i].setData([]);
                    }
                  }
                });
                e.preventDefault();
                e.stopPropagation();
              }
            })
          });
        </script>
      </head>
      <body>
        <script src="http://localhost/puskesmas/statistik/highcharts.js"></script>
        <script src="http://localhost/puskesmas/statistik/js/modules/exporting.js"></script>
        <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
      </body>
    </div>
      <a href="_statistik.php" style="float:right;margin-right: 15px;"><input type="button" value="<<kembali" class="btn btn-success"></a>
    </div>
  </div>
  <div class="box" id="content-box2"></div><br class="clearfix" />
  </div>
  <div id="sidebar">
  <br class="clearfix" />
</div>
<?php include('footer.php');?>