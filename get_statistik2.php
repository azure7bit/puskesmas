<?php
  header('Content-Type: application/json');
  include('koneksi.php');

  $tahun = $_POST['tahun'];
  $bulan = $_POST['bulan_awal'];
  $bulan_2 = $_POST['bulan_akhir'];

  $bulan = (strlen($bulan) == 1) ? $bulan : "0" . $bulan;
  $bulan_2 = (strlen($bulan_2) == 1) ? $bulan_2 : "0" . $bulan_2;

  $qry = mysql_query("SELECT tbl1.nama_penyakit, tbl1.penyakit_group,
    IF(tbl2.penyakit_group IS NULL or tbl2.penyakit_group = '', 0, tbl2.penyakit_group),
    IF(tbl3.penyakit_group IS NULL or tbl3.penyakit_group = '', 0, tbl3.penyakit_group) FROM ((SELECT COUNT(penyakit)
    AS penyakit_group, pasien.jkelamin AS jkelamin, penyakit AS nama_penyakit FROM rekam_medis INNER JOIN pasien
    ON rekam_medis.noregistrasi = pasien.noregistrasi WHERE YEAR(`tglperiksa`)='$tahun'
    AND MONTH(`tglperiksa`) BETWEEN '$bulan' AND '$bulan_2' GROUP BY nama_penyakit ORDER BY penyakit_group
    DESC LIMIT 0,5)as tbl1 )

    left join ((SELECT COUNT(penyakit)
    AS penyakit_group, pasien.jkelamin AS jkelamin, penyakit AS nama_penyakit FROM rekam_medis INNER JOIN pasien
    ON rekam_medis.noregistrasi = pasien.noregistrasi where jkelamin='P' and YEAR(`tglperiksa`)='$tahun'
    AND MONTH(`tglperiksa`) BETWEEN '$bulan' AND '$bulan_2' GROUP BY nama_penyakit ORDER BY penyakit_group
    )as tbl2) on tbl2.nama_penyakit = tbl1.nama_penyakit

    left join ((SELECT COUNT(penyakit)
    AS penyakit_group, pasien.jkelamin AS jkelamin, penyakit AS nama_penyakit FROM rekam_medis INNER JOIN pasien
    ON rekam_medis.noregistrasi = pasien.noregistrasi where jkelamin='L' and YEAR(`tglperiksa`)='$tahun'
    AND MONTH(`tglperiksa`) BETWEEN '$bulan' AND '$bulan_2' GROUP BY nama_penyakit ORDER BY penyakit_group
    )as tbl3) on tbl3.nama_penyakit = tbl1.nama_penyakit ");

  $rows = array();
  $rows1 = array();
  $rows2 = array();

  while ($r = mysql_fetch_array($qry)) {
    $rows['name'] = 'L';
    $rows1['name'] = 'P';
    $rows2['name'] = '(L+P)';
    $rows['category'][] = $r['nama_penyakit'];
    $rows['data'][] = $r[3];
    $rows1['data'][] = $r[2];
    $rows2['data'][] = $r[1];
  }

  $result = array();
  array_push($result,$rows);
  array_push($result,$rows1);
  array_push($result,$rows2);

  $json = json_encode($result, JSON_NUMERIC_CHECK);
  echo $json;

?>