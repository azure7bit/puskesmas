<?php
  require_once("paging/config.php");
  require_once("paging/query.php");

  include ('koneksi.php');
  session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<script language="JavaScript" type="text/javascript">
  function delpasien(a){
    if (confirm("delete data pasien?")){
      window.location.href = 'registrasi.php?delpasien=' + a;
    }
  }
</script>

<div>
  <div>
    <div>
      <div style="padding:25px">
        <div id="search_pasien">
          <form name="search_pasien" method="POST" action="search_pasien.php" role="form">
            <table width="35%">
              <tr>
                <h4> Cari Pasien <h4>
                <td><input type="text" name="nama_pasien" value="<?php echo $nama_pasien;?>" class="form-control"></td>
                <td><input type="submit" value="search" class="btn btn-success"></td>
              </tr>
            </table>
          </form>
        </div>
      <div id="tampilanperhalaman" style="margin-top: 5px;">
        <div style="text-align:right" style="margin-top: 5px;">
          <button class="btn btn-success" onclick="window.location='add_pasien.php'">Tambah Pasien Baru</button>
        </div>
        <?php
          include('koneksi.php');
          $nama_pasien = $_POST['nama_pasien'];
          $sql_pasien = mysql_query("select * from pasien where namaPasien like '%$nama_pasien%' or noRegistrasi like '$nama_pasien' ORDER BY noRegistrasi DESC") or die(mysql_error());
          if(mysql_num_rows($sql_pasien) < 1){
            echo "Tidak ditemukan data yang mirip dengan : \"$nama_pasien\" <br> <br>";
          } else {
          echo "Ditemukan data yang mirip dengan : \"$nama_pasien\" ";
        ?>
        <table width="100%" class="table table-striped" style="margin-top: 22px;" >
          <thead>
            <tr>
              <th width="2%" style="font-size: 13px;text-align:center">No</th>
              <th width="5%" style="font-size: 13px;text-align:center">No. Registrasi</th>
              <th width="5%" style="font-size: 13px;text-align:center">Tanggal Pendaftaran</th>
              <th width="20%" style="font-size: 13px;text-align:center">Nama Pasien</th>
              <th width="25%" style="font-size: 13px;text-align:center">Alamat</th>
              <th width="3%" style="font-size: 13px;text-align:center">L/P</th>
              <th width="10%" style="font-size: 13px;text-align:center">Tanggal Lahir</th>
              <th width="25%" style="font-size: 13px;text-align:center">Aksi</th>
            </tr>
          </thead>
          <?php
            $counter=$start;
            while($row=mysql_fetch_array($sql_pasien)){
              $a = $row['noRegistrasi'];
              $b = $row['tglReg'];
              $c = $row['namaPasien'];
              $d = $row['Alamat'];
              $e = $row['jkelamin'];
              $f = $row['tglLahir'];
              $flag= $row['flag'];
              $status = '';
              if($flag == '0'){
                $status = "<a href=periksa.php?id=".$a.">Periksa</a>";
              } else if($flag == '1'){
                $status = "Sedang Periksa";
              }
              $counter++;
              echo "<tr><td>$counter</td><td>$a</td><td>$b</td><td>$c</td><td style=font-size:9px>$d</td><td>$e</td><td style=font-size:11px>$f</td><td style=color:#0F8C8C>
                <a href=edit_pasien.php?id=".$a.">Edit</a> | <a href=\"javascript:delpasien('$a')\">Delete</a> | ".$status."</td></tr>";
            }
          ?>
        <tbody align="" role="alert" aria-live="polite" aria-relevant="all"></tbody>
        </table>
        <?php } ?>
      </div>
    </div>
  </div>
  <br class="clearfix" />
</div>
<?php include('footer.php');?>