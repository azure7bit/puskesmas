<?php
  include ('koneksi.php');
  session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }

  include('header.php');

  if (isset ($_GET['err'])) {
    switch ($_GET['err']) {
      case 0 : echo 'Sukses';
      break;
      case 1 : echo 'nomor Sudah Terdaftar';
      break;
      case 2 : echo 'Silahkan registrasi dulu';
      break;
      default : echo '';
      break;
    }
  }

  if(isset($_GET['page'])) $page = $_GET['page'];
  else $page=1;

  $s = 20;
  $x = ($s * $page)+1;
  if($page==1){$x=1;}
  $jml = mysql_query("SELECT * FROM pasien");
  $n = mysql_num_rows($jml);

  $y = ($s * $page);
  if ($y<=$n) {$y=$x+$s-1;}
  else $y=$n;
?>

<script type="text/javascript">
  function validate(){
    if (document.registrasi.nama.value == ''){
      alert('Nama tdk boleh kosong');
      document.registrasi.nama.focus();
      return false;
    } else if (document.registrasi.nomor.value == ''){
      alert('nomor tdk boleh kosong');
      document.registrasi.nomor.focus();
      return false;
    } else if (document.registrasi.email.value == ''){
      alert('email tdk boleh kosong');
      document.registrasi.email.focus();
      return false;
    }
  }
</script>

<div>
  <div id="content" style="width:auto;">

    <div class="clearfix">

    <!-- tambah data pasien -->
      <div id="kotak_box">
        <h4>Tambah data pasien</h4>
        <table width="90%">
          <form action="proses/pasien.php" method="post" name="add_pasien">
            <tr>
              <td>Nama Pasien</td>
              <td><input type="text" name="nama_pasien"></td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td><input type="text" name="alamat"></td>
            </tr>
            <tr>
              <td>Jenis kelamin</td>
              <td>
                <input type="radio" name="jenkel" value="L" style="position: relative;top: -3px;">Laki-laki
                <input type="radio" name="jenkel" value="P" style="position: relative;top: -3px;">Perempuan
              </td>
            </tr>
            <tr>
              <td>Tanggal lahir</td>
              <td>
                <select name="day">
                  <option value="">Tanggal</option>"
                  <?php
                    for($i=1;$i<=31;$i++){
                      echo "<option value=".$i.">".$i."</option>";
                    }
                  ?>
                </select>

                <select name="month">
                  <option value="">Bulan</option>
                  <option value="01">Januari</option>
                  <option value="02">Febuari</option>
                  <option value="03">Maret</option>
                  <option value="04">April</option>
                  <option value="05">Mei</option>
                  <option value="06">Juni</option>
                  <option value="07">Juli</option>
                  <option value="08">Augustus</option>
                  <option value="09">September</option>
                  <option value="10">Oktober</option>
                  <option value="11">November</option>
                  <option value="12">Desember</option>
                </select>

                <select name="year">
                  <option value=''>Tahun</option>
                  <?php
                  $tahun = date("Y");
                    for($j=1945;$j<=$tahun;$j++){
                      echo "<option value=".$j.">".$j."</option>";
                    }
                   ?>
                </select>
              </td>
            </tr>
            <tr>
              <td colspan="2"><input type="submit" name="add_pasien" value="tambah pasien" class="btn btn-success">
              <a href="registrasi.php" style="float:right;margin-right: 15px;"><input type="button" value="cancel" class="btn btn-info"></a>
            </tr>
          </form>
        </table>
      </div>
      <br class="clearfix" />
    </div>
    <?php include('footer.php');?>