<?php
  header('Content-Type: application/json');
  include('koneksi.php');

  $tahun = $_POST['tahun'];
  $bulan = $_POST['bulan_awal'];
  $bulan_2 = $_POST['bulan_akhir'];

  $result = array();
  $serie = array();
  $bulan = (strlen($bulan) == 1) ? $bulan : "0" . $bulan;
  $bulan_2 = (strlen($bulan_2) == 1) ? $bulan_2 : "0" . $bulan_2;

  $qry = mysql_query("SELECT count(*) AS total, MONTH(`tglperiksa`) as bulan, CASE
    WHEN MONTH(`tglperiksa`) = 1 THEN 'January'
    WHEN MONTH(`tglperiksa`) = 2 THEN 'February'
    WHEN MONTH(`tglperiksa`) = 3 THEN 'March'
    WHEN MONTH(`tglperiksa`) = 4 THEN 'April'
    WHEN MONTH(`tglperiksa`) = 5 THEN 'May'
    WHEN MONTH(`tglperiksa`) = 6 THEN 'June'
    WHEN MONTH(`tglperiksa`) = 7 THEN 'July'
    WHEN MONTH(`tglperiksa`) = 8 THEN 'August'
    WHEN MONTH(`tglperiksa`) = 9 THEN 'September'
    WHEN MONTH(`tglperiksa`) = 10 THEN 'October'
    WHEN MONTH(`tglperiksa`) = 11 THEN 'November'
    WHEN MONTH(`tglperiksa`) = 12 THEN 'December'
    END as nam_bulan FROM rekam_medis
    WHERE YEAR(`tglperiksa`)='$tahun' AND MONTH(`tglperiksa`) BETWEEN '$bulan'
    AND '$bulan_2' GROUP BY bulan");

  while ($row_ttl = mysql_fetch_array($qry)) {
    $serie['name'] = 'Total';
    $serie['category'][] = $row_ttl['nam_bulan'];
    $serie['data'][] = $row_ttl['total'];
  }

  array_push($result, $serie);

  $json = json_encode($result, JSON_NUMERIC_CHECK);
  echo $json;

?>