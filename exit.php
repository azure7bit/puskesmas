<?php
  include ('koneksi.php');
  session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<div id="page">
  <div id="content">
    <div class="box">
      <h2>Form Pasien Keluar</h2>
    </div>
    <div class="box" id="content-box1">
      <nav><ul class="sf-menu" id="nav">
        <a href="#">
          <?php
            $qry = mysql_query("SELECT nomor from periksa where menunggu='tidak menunggu' order by nomor desc");
            while($row = mysql_fetch_array($qry)) {
              $nomor = $row['nomor'];
              $qry2 = mysql_query("SELECT namaPasien from pasien where nomor='$nomor'");
              $nama = mysql_fetch_array($qry2);
              echo '<li class="sf-parent"><b><a href="exit.php?nomor='.$nomor.'">'.$nama['namaPasien'].'</a></b><br />';
              echo '<ul>';
              echo '<li>';
                $nomor = $row['nomor'];
                $qry2 = mysql_query("SELECT namaPasien from pasien where nomor='$nomor'");
                $nama = mysql_fetch_array($qry2);
                $getInfoQ = mysql_query("SELECT tdarah,gejala FROM periksa WHERE nomor = '$nomor'");
                $getInfo = mysql_fetch_array($getInfoQ);
                echo 'Gejala : '.$getInfo['gejala'].'<br />';
                echo 'Tekanan Darah : '.$getInfo['tdarah'].'<br />';
              echo '</li>';
              echo '</ul>';
            }
            echo '</li>';
          ?>
        </a>
      </nav>
    </div>
    <div class="box" id="content-box2">
      <form id="form1" name="exit" method="post" action="enter.php">
        <?php
          if (!isset($_REQUEST['nomor'])){
            $nomor = null;
          } else {
            $nomor = $_REQUEST['nomor'];
          }
          if ($nomor == ''){
            $nomor = '';
          } else {
            $nomor = $_REQUEST['nomor'];
          }
        ?>
        <input type="text" name="nomor" id="taruhnomor" value="<?php echo $nomor;?>" />
        <p>Penyakit</p><input name='penyakit' type="text" size="57" />
        <p>Obat</p><textarea rows="12" cols="40" name="obat" type="text"> </textarea>
        <p>Istirahat? </p>
        <p>
          <input type="radio"name="lamaijin"value="1"id="lamaijin_0"/>1 Hari <br />
          <input type="radio"name="lamaijin"value="2"id="lamaijin_1"/>2 Hari <br />
          <input type="radio"name="lamaijin"value="3"id="lamaijin_2"/>3 Hari <br />
        </p>
        <input type="submit" name="exit" value="exit" />
      </form>
    </div>
    <br class="clearfix" />
  </div>

  <div id="sidebar">
    <div class="box"></div>
    <div class="box"></div>
  </div>
  <br class="clearfix" />
</div>

<!-- javascript at the bottom for fast page loading -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easing-sooper.js"></script>
<script type="text/javascript" src="js/jquery.sooperfish.js"></script>
<script type="text/javascript" src="js/image_slide.js"></script>
<!-- initialise sooperfish menu -->
<script type="text/javascript">
  $(document).ready(function() {
    $('ul.sf-menu').sooperfish();
  });
</script>

<?php include('footer.php');?>