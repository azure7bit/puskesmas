<?php
 ini_set('display_errors', 'On');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  if ($_POST['mydropdown']== "satu"){
    if ($_POST['bulan_awal'] && $_POST['bulan_akhir']){
      if($_POST['bulan_awal'] < $_POST['bulan_akhir']){
        $con=mysqli_connect("localhost","root","","puskesmas");
        // Check connection
        if (mysqli_connect_errno()){
          echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        $tahun = $_POST['tahun'];
        $bulan = $_POST['bulan_awal'];
        $bulan_2 = $_POST['bulan_akhir'];

        for($i=$_POST['bulan_awal'];$i<=$_POST['bulan_akhir'];$i++){
          $bulan = (strlen($i) == 1) ? "0" . $i : $i ;
          $build_date = $_POST['tahun'] . "-" .$bulan;
          $qry = mysqli_query($con,"SELECT count(*) AS total FROM rekam_medis WHERE TglPeriksa LIKE '$build_date%' LIMIT 0,1");
          $row_ttl = mysqli_fetch_array($qry);
          $month = date("F",strtotime($build_date . "-01 00:00:00" ));
          $arr_total_pas[$month] = $row_ttl['total'];
        }
        include '_statistik_result_1.php';
      }
      else{echo "Bulan awal tidak boleh lebih besar daripada bulan akhir";}
    }
  }

  if ($_POST['mydropdown']== "dua"){
    if ($_POST['bulan_awal'] && $_POST['bulan_akhir']){
      if($_POST['bulan_awal'] <= $_POST['bulan_akhir']){
        include '_statistik_result_2.php';
      }
      else{echo "Bulan awal tidak boleh lebih besar daripada bulan akhir";}
    }
  }

  if ($_POST['mydropdown']== "tiga"){
    if ($_POST['bulan_awal'] && $_POST['bulan_akhir']){
      include "_statistik_result_3.php";
    }else{echo "Bulan awal tidak boleh lebih besar daripada bulan akhir";}
  }

  if ($_POST['mydropdown']== "empat"){
    if ($_POST['bulan_awal'] && $_POST['bulan_akhir']){
      if($_POST['bulan_awal'] <= $_POST['bulan_akhir']){
        $con=mysqli_connect("localhost","root","","puskesmas");

        // Check connection
        if (mysqli_connect_errno()){
          echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        $tahun = $_POST['tahun'];
        $bulan = $_POST['bulan_awal'];
        $bulan_2 = $_POST['bulan_akhir'];

        for($i=$_POST['bulan_awal'];$i<=$_POST['bulan_akhir'];$i++){
          $bulan = (strlen($i) == 1) ? "0" . $i : $i ;
          $build_date = $_POST['tahun'] . "-" .$bulan;
          $qry = mysqli_query($con,"SELECT count(*) as total, poliklinik FROM rekam_medis WHERE TglPeriksa LIKE '$build_date%' GROUP BY poliklinik");
          $row_ttl = mysqli_fetch_array($qry);
          $month = date("F",strtotime($build_date . "-01 00:00:00" ));
          $arr_total_pas[$month] = $row_ttl['total'];
          include "_statistik_result_4.php";
        }
      }
    }else{echo "Bulan awal tidak boleh lebih besar daripada bulan akhir";}
  }
?>