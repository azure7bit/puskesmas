<?php
  include ('koneksi.php');
  session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<div id="page">
  <div id="content">
    <div id="splash">
      <h4>Selamat Datang, </h4>
      <h4>Anda masuk sebagai akun <?php echo $_SESSION['adminname']?>,  </h4>
      <img class="pic" src="images/pic01.jpg" width="870" height="100" alt="" />
    </div>
  </div>
  <div id="sidebar">
    <div class="box">
    </div>
    <div class="box">
    </div>
  </div>
  <br class="clearfix" />
</div>

<?php include('footer.php');?>