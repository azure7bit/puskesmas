<?php
  include ('koneksi.php');
  session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');

  $id_pasien = $_GET['id'];

  $sql = mysql_query("SELECT * FROM pasien WHERE noRegistrasi = '$id_pasien'");

  $nama_pasien  = '';
  $alamat     = '';
  $jenkel     = '';
  $yyyy_mm_dd   = '';

  while ($line = mysql_fetch_array($sql)) {
    $nama_pasien =  $line['namaPasien'];
    $alamat =  $line['Alamat'];
    $jenkel =  $line['jkelamin'];
    $yyyy_mm_dd = $line['tglLahir'];
  }

  //cara memecah yyyy-mm-dd ke 3 variabel yg berbeda ://
  $yyyy; //utk tahun, misalnya 1990
  $mm; //utk bulan, misalnya 05
  $dd; //utk day, misalnya 15

  //contoh explode dari 1965-05-15: hasil yg dibentuk dari exlode yaitu array [0] = tahun, [1] = bulan, [2] = hari
  $array_hasil_pecah = explode('-',$yyyy_mm_dd);

  $yyyy = $array_hasil_pecah[0];
  $mm = $array_hasil_pecah[1];
  $dd = $array_hasil_pecah[2];

?>

<div>
  <div id="content" style="width:auto;">
    <div class="clearfix">

      <!-- tambah data pasien -->
      <div id="kotak_box">
        <h4>Edit data pasien</h4>
        <table width="90%">
          <form action="proses/pasien.php" method="post" name="edit_pasien">
          <tr>
            <td colspan = "2"><input type="hidden" name="id_pasien" value="<?php echo $id_pasien;?>"></td>
          </tr>
          <tr>
            <td>Nama Pasien</td>
            <td><input type="text" name="nama_pasien" value="<?php echo $nama_pasien;?>"></td>
          </tr>
          <tr>
            <td>Alamat</td>
            <td><input type="text" name="alamat" value="<?php echo $alamat;?>"></td>
          </tr>
          <tr>
            <td>Jenis kelamin</td>
            <td id="kelamin">
              <?php
                $check_L = '';
                $check_P = '';

                if ($jenkel == 'L'){
                  $check_L = 'checked="checked"';
                } elseif ($jenkel == 'P'){
                  $check_P = 'checked="checked"';
                }
              ?>
              <input type="radio" <? echo $check_L ?>  name="jenkel" value="L" style="position: relative;top: -3px;">Laki-laki
              <input type="radio" <? echo $check_P ?> name="jenkel" value="P" style="position: relative;top: -3px;">Perempuan
            </td>
          </tr>
          <tr>
            <td>Tanggal lahir</td>
            <td>
              <select id="day" name="day">
                <option value="">Tanggal</option>"
                <?php
                  for($i=1;$i<=31;$i++){
                    echo "<option value=".$i.">".$i."</option>";
                  }
                ?>
              </select>

              <select id="month" name="month">
                <option value="">Bulan</option>
                <option value="01">Januari</option>
                <option value="02">Febuari</option>
                <option value="03">Maret</option>
                <option value="04">April</option>
                <option value="05">Mei</option>
                <option value="06">Juni</option>
                <option value="07">Juli</option>
                <option value="08">Augustus</option>
                <option value="09">September</option>
                <option value="10">Oktober</option>
                <option value="11">November</option>
                <option value="12">Desember</option>
              </select>

              <select id="year" name="year">
                <option value=''>Tahun</option>
                <?php
                  $tahun = date("Y");
                  for($j=1945;$j<=$tahun;$j++){
                    echo "<option value=".$j.">".$j."</option>";
                  }
                ?>
              </select>
            </td>
          </tr>
          <tr>
            <td colspan="2"><input type="submit" name="edit_pasien" value="edit pasien" class="btn btn-success">
            <a href="registrasi.php" style="float:right;margin-right: 15px;"><input type="button" value="cancel" class="btn btn-info"></a>
          </tr>
        </form>
      </table>
    </div>

    <div id="sidebar">
      <div class="box"></div>
      <div class="box"></div>
    </div>
    <br class="clearfix" />
  </div>

  <!--CARA MENULIS JAVASCRIPT/ JQUERY--->
  <script>
    $('#year').val('<?php echo $yyyy; ?>');
    $('#month').val('<?php echo $mm; ?>');
    $('#day').val('<?php echo $dd; ?>');
  </script>

  <?php include('footer.php');?>