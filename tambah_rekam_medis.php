<?php
  include ('koneksi.php');
  session_start();
  if (!isset($_SESSION['adminsession'])){
      header("location:login.php");
  }
  include('header.php');
?>

<div class="clearfix">
  <!-- tambah data pasien -->
  <div id="kotak_box">
    <?php
      $id_pasien = $_GET['id'];
      $result = mysql_query("select * from pasien where noRegistrasi = '$id_pasien'");
      while($row=mysql_fetch_array($result)){
        echo "<h4>Tambah Data Rekam Medis Pasien :   ".$row['namaPasien']."</h4>";
      }
    ?>
    <table width="90%">
      <form action="proses/pasien.php" method="post" name="tambah_rekam_medis">
        <tr>
          <td>Penyakit</td>
          <td><input type="text" name="penyakit"></td>
        </tr>
        <tr>
          <td>Obat / Tindakan</td>
          <td><textarea name="obat"> </textarea></td>
        </tr>
        <tr>
          <td colspan="2"><input type="hidden" name="NoRegistrasi" value="<?php echo $id_pasien;?>">
        </tr>
        <tr>
          <td colspan="2"><input type="submit" name="tambah_rekam_medis" value="simpan" class="btn btn-primary">
          <a href="rekam_medis.php?id=<?php echo $id_pasien;?>" style="float:right;margin-right: 15px;">
          <input type="button" value="<<kembali" class="btn btn-info"></a>
        </tr>
      </form>
    </table>
  </div>
  <br class="clearfix" />
</div>
<?php include('footer.php');?>