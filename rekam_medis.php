<?php
  include ('koneksi.php');
  @session_start();
  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }
  include('header.php');
?>

<div id="page">
  <div id="content">
    <div class="box">
      <?php
        if ($_SESSION['level'] != 'dokter') {
          header("location:login.php");
        }
      ?>
      <div class="box" id="content-box1">
        <h4>Rekam Medis Pasien </h4>
        <br />
        <table width="25%">
          <?php
            $id_pasien = $_GET['id'];
            $result = mysql_query("select * from pasien where noRegistrasi = '$id_pasien'");
            while($row=mysql_fetch_array($result)){
              echo "<tr>
                  <td>No Registrasi </td> <td>".$row['noRegistrasi']."</td>
                  </tr>
                  <br />
                  <tr>
                    <td>Nama  </td> <td> ".$row['namaPasien']."</td>
                  </tr>";
            }
          ?>
        </table>
        <hr />

        <table class="table table-striped" style="width:60%">
          <thead>
            <tr>
              <th style="font-size: 13px;text-align:center">No</th>
              <th style="font-size: 13px;text-align:center">Waktu</th>
              <th style="font-size: 13px;text-align:center">Penyakit</th>
              <th style="font-size: 13px;text-align:center">Obat</th>
            </tr>
          </thead>
          <?php
            @$counter=$start;
            $result2 = mysql_query("select * from rekam_medis where noRegistrasi = '$id_pasien' ORDER BY TglPeriksa ASC");
            while($row2=mysql_fetch_array($result2)){
              $penyakit = $row2['Penyakit'];
              $obat     = $row2['Obat'];
              $tglperiksa = $row2['TglPeriksa'];
              $counter++;
              echo "<tr><td>$counter</td><td>$tglperiksa</td><td>$penyakit</td><td>$obat</td></tr>";
            }
          ?>
          <tbody align="" role="alert" aria-live="polite" aria-relevant="all"></tbody>
        </table>
        <a href="tambah_rekam_medis.php?id=<?php echo $id_pasien;?>" style="float:left;margin-right: 15px;"><input type="button" value="Tambah" class="btn btn-primary"></a>
        <a href="proses/selesai_rekam_medis.php?id=<?php echo $id_pasien;?>"  style="float:right"><input type="button" value="Selesai" class="btn btn-success"></a>
      </div>
    </div>
    <a href="enter.php" style="float:right"><input type="button" value="<<kembali" class="btn btn-info"></a>
  </div>
</div>

<br class="clearfix" />
<br class="clearfix" />

<?php include('footer.php');?>