<?php
  session_start();

  if(isset($_SESSION['adminsession'])){
    unset($_SESSION['adminsession']);
  }
  session_destroy();
  header("Location: login.php?mode=logout");
?>