<?php
  include ('koneksi.php');

  @session_start();

  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }

  if (isset($_GET['poliklinik'])){
    $poliklinik=$_GET['poliklinik'];
  }else{
    if (($_SESSION['adminname'])=='Poliklinik Umum'){
      $poliklinik="Umum";
    } elseif (($_SESSION['adminname'])=='Poliklinik Lansia'){
      $poliklinik="Lansia";
    } elseif (($_SESSION['adminname'])=='Poliklinik Gigi'){
      $poliklinik="Gigi";
    } elseif (($_SESSION['adminname'])=='Poliklinik KIA/KB'){
      $poliklinik="KIA/KB";
    } else{
      $poliklinik="semua";
    }
  }
  include('header.php');

?>
<div>
  <div>
    <div class="box">
    <div>

      <div style="padding:25px">
        <?php if ($_SESSION['level'] != 'dinas') { echo $_SESSION['adminname'] ?>
        <div class="box" id="content-box1">
          <div><h4>LIST MENUNGGU : </h4></div>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="2%" style="font-size: 13px;text-align:center">No</th>
                  <th width="5%" style="font-size: 13px;text-align:center">No. Registrasi</th>
                  <th width="5%" style="font-size: 13px;text-align:center">Nama Pasien</th>
                  <th width="15%" style="font-size: 13px;text-align:center">Alamat</th>
                  <th width="5%" style="font-size: 13px;text-align:center">Tanggal Lahir</th>
                  <th width="5%" style="font-size: 13px;text-align:center">Poliklinik</th>
                </tr>
              </thead>
              <?php
                @$counter=$start;
                $result = mysql_query("SELECT * FROM pasien pa, periksa pe WHERE pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");

                if ($poliklinik=='Umum'){
                  $result = mysql_query("SELECT * FROM pasien pa, periksa pe WHERE pe.poliklinik='Umum' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
                } elseif ($poliklinik=='Lansia'){
                  $result = mysql_query("SELECT * FROM pasien pa, periksa pe WHERE pe.poliklinik='Lansia' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
                } elseif ($poliklinik=='Gigi'){
                  $result = mysql_query("SELECT * FROM pasien pa, periksa pe WHERE pe.poliklinik='Gigi' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
                } elseif ($poliklinik=='KIA/KB'){
                  $result = mysql_query("SELECT * FROM pasien pa, periksa pe WHERE pe.poliklinik='KIA/KB' AND pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
                } else{
                  $result = mysql_query("SELECT * FROM pasien pa, periksa pe WHERE pe.noRegistrasi = pa.noRegistrasi AND pa.flag = '1' ORDER BY pa.date_update_flag ASC");
                }

                while(@$row=mysql_fetch_array($result)){
                  if ($_SESSION['level'] == 'dokter'){
                    $a = "<a href=rekam_medis.php?id=".$row['noRegistrasi'].">".$row['noRegistrasi']."</a>";
                    $b = "<a href=rekam_medis.php?id=".$row['noRegistrasi'].">".$row['namaPasien']."</a>";
                    $c = $row['Alamat'];
                    $f = $row['tglLahir'];
                    $g = $row['poliklinik'];
                    $counter++;
                    echo "<tr><td>$counter</td><td>$a</td><td>$b</td><td>$c</td><td style='text-align:center'>$f </td><td style='text-align:center'>$g</td></tr>";
                  } elseif ($_SESSION['level'] == 'admin'){
                    $a = $row['noRegistrasi'];
                    $b = $row['namaPasien'];
                    $c = $row['Alamat'];
                    $f = $row['tglLahir'];
                    $g = $row['poliklinik'];
                    $counter++;
                    echo "<tr><td>$counter</td><td>$a</td><td>$b</td><td>$c</td><td style='text-align:center'>$f </td><td style='text-align:center'>$g</td></tr>";
                  }
                }
              ?>
              <tbody align="" role="alert" aria-live="polite" aria-relevant="all"></tbody>
            </table>
          </div>
        </div>
      </div>
    <?php } else { echo "Maaf Anda tidak bisa akses menu Pasien Masuk"; } ?>
  </div>
</div>
</div>
<div id="sidebar">
</div>

<br class="clearfix" />
<br class="clearfix" />

<script type="text/javascript">
  function goToNewPage(dropdownlist){
   var url = dropdownlist.options(dropdownlist.selectedIndex).value;
   if (url != ""){
    window.open(url);}
   }
</script>

<?php include('footer.php');?>