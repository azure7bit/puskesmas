<?php
  session_start();

  include ('koneksi.php');

  require_once("paging/config.php");
  require_once("paging/query.php");

  if (!isset($_SESSION['adminsession'])){
    header("location:login.php");
  }

  include('header.php');

  if(isset($_GET['delpasien'])){
    $query = mysql_query("DELETE FROM pasien WHERE noRegistrasi = '{$_GET['delpasien']}'")or die('Error : ' . mysql_error());
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
  }

?>

<script language="JavaScript" type="text/javascript">
  function delpasien(a){
    if (confirm("delete data pasien?")){
      window.location.href = 'registrasi.php?delpasien=' + a;
    }
  }
</script>

<div>
  <div>
    <div class="box">
      <div>
        <div style="padding:25px">
          <?php if ($_SESSION['level'] != 'dinas') { ?>
          <div id="search_pasien">
            <form name="search_pasien" method="POST" action="search_pasien.php" role="form">
              <table width="35%">
                <tr>
                  <h4> Cari Pasien <h4>
                  <td><input type="text" name="nama_pasien" class="form-control" placeholder="Nama/No.Registrasi"> </td>
                  <td><input type="submit" value="search" class="btn btn-success"></td>
                </tr>
              </table>
            </form>
          </div>
          <div id="tampilanperhalaman" style="margin-top: 5px;">
            Records Per Page:
            <?=pagesize_dropdown('pagesize', $pagesize);?>
            <div style="float:right">
              <button class="btn btn-success" onclick="window.location='add_pasien.php'">Tambah Pasien Baru</button>
            </div>
            <table class="table table-striped">
              <thead>
                <tr>
                  <th width="2%" style="font-size: 13px;text-align:center">No</th>
                  <th width="5%" style="font-size: 13px;text-align:center">No. Registrasi</th>
                  <th width="5%" style="font-size: 13px;text-align:center">Tanggal Pendaftaran</th>
                  <th width="20%" style="font-size: 13px;text-align:center">Nama Pasien</th>
                  <th width="25%" style="font-size: 13px;text-align:center">Alamat</th>
                  <th width="3%" style="font-size: 13px;text-align:center">L/P</th>
                  <th width="10%" style="font-size: 13px;text-align:center">Tanggal Lahir</th>
                  <th width="25%" style="font-size: 13px;text-align:center">Aksi</th>
                </tr>
              </thead>
              <?php
                $counter=$start;
                while($row=mysql_fetch_array($result)){
                  $a = $row['noRegistrasi'];
                  $b = $row['tglReg'];
                  $c = $row['namaPasien'];
                  $d = $row['Alamat'];
                  $e = $row['jkelamin'];
                  $f = $row['tglLahir'];
                  $flag= $row['flag'];
                  $status = '';
                  if($flag == '0'){
                    $status = "<a href=periksa.php?id=".$a.">Periksa</a>";
                  } else if($flag == '1'){
                    $status = "Sedang Periksa";
                  }
                  $counter++;
                  echo "<tr><td>$counter</td><td>$a</td><td>$b</td><td>$c</td><td style=font-size:9px>$d</td><td>$e</td><td style=font-size:11px>$f</td><td style=color:#0F8C8C;>
                  <a href=edit_pasien.php?id=".$a.">Edit</a> | <a href=\"javascript:delpasien('$a')\"> Delete</a> | ".$status." </td></tr>";
                }
              ?>
              <tbody align="" role="alert" aria-live="polite" aria-relevant="all"></tbody>
            </table>
            <div id="tampilanperhalaman" style="margin-top: 5px;">
              <strong>Showing Records <?= $start+1?>&nbsp;to <?=($reccnt<=$start+$pagesize)?($reccnt):($start+$pagesize)?> of <?=$reccnt?>
              </strong>
            </div>
            <div id="paging">
              <?php include_once("paging/paging.inc.php");  ?>
            </div>
          </div>
        </div>
        <?php } else { echo "Maaf Anda tidak bisa akses menu Registrasi Pasien";  } ?>
      </div>
    </div>
  </div>
  <div id="sidebar"></div>
</div><br class="clearfix" /><br class="clearfix" />
<?php include('footer.php');?>