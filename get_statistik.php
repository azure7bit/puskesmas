<?php
  header('Content-Type: application/json');
  include('koneksi.php');

  $tahun = $_POST['tahun'];
  $bulan = $_POST['bulan_awal'];
  $bulan_2 = $_POST['bulan_akhir'];

  $result = array();
  $serie = array();

  $bulan = (strlen($bulan) == 1) ? $bulan : "0" . $bulan;
  $bulan_2 = (strlen($bulan_2) == 1) ? $bulan_2 : "0" . $bulan_2;

  $qry = mysql_query(
    "SELECT COUNT(*) as total,
      CASE
        WHEN age >= 0 AND age <= 5 THEN 'Balita (0-5)'
        WHEN age >5 AND age <=10 THEN 'Anak-anak (5-10)'
        WHEN age >10 AND age <=20 THEN 'Remaja (10-20)'
        WHEN age >20 AND age <= 30 THEN 'Dewasa1 (20-30)'
        WHEN age >30 AND age <=40 THEN 'Dewasa2 (30-40)'
        WHEN age >40 AND age <=50 THEN 'Dewasa3 (40-50)'
        WHEN age >50 AND age <=55 THEN 'Lansia1 (50-55)'
        WHEN age >55 THEN 'Lansia1 (55-...)'
      END AS nama_umur
      FROM
      (
        SELECT YEAR(CURDATE()) - YEAR(`pasien`.`tglLahir`) as age
          FROM `pasien`
          INNER JOIN `rekam_medis` ON `pasien`.`noRegistrasi` = `rekam_medis`.`noRegistrasi`
          WHERE YEAR(`tglperiksa`)='$tahun' AND MONTH(`tglperiksa`) BETWEEN '$bulan' AND '$bulan_2'
      ) as tbl
      GROUP BY nama_umur"
    );

  while ($row_ttl = mysql_fetch_array($qry)) {
    $arr_total_pas[$month] = $row_ttl['total'];
    $serie[] = array($row_ttl['nama_umur'], $row_ttl['total']);
  }

  array_push($result, $serie);

  $arr_nama_nama_bulan = "";
  foreach($arr_total_pas as $nama => $val){
    $arr_nama_nama_bulan[] = $nama;
  }

  $json = json_encode($serie, JSON_NUMERIC_CHECK);
  echo $json;

?>